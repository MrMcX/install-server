#!/bin/bash

IP=`hostname -I | cut -d" " -f1`
RANGE=`echo $IP | cut -d"." -f1-3`.0
read -p "Cloud User: " USER
read -sp "Cloud Password: " PASSWORD

# Install and configure server
apt install -y dnsmasq pxelinux nfs-kernel-server
echo "dhcp-range=$RANGE,proxy			
dhcp-boot=pxelinux.0,$IP,$RANGE
pxe-service=x86PC,\"Computertruhe Network Boot\",pxelinux
enable-tftp
tftp-root=/var/lib/tftpboot" > /etc/dnsmasq.conf
mkdir -p /var/lib/tftpboot/pxelinux.cfg
cp /usr/lib/PXELINUX/pxelinux.0 /var/lib/tftpboot/ 
cp /usr/lib/syslinux/memdisk /var/lib/tftpboot/ 
cp -v /usr/lib/syslinux/modules/bios/{ldlinux.c32,linux.c32,libcom32.c32,libutil.c32,menu.c32,vesamenu.c32} /var/lib/tftpboot/
echo "/var/lib/tftpboot $RANGE/255.255.255.0(ro,async,no_subtree_checks)" >> /etc/exports && exportfs -a

# Configure Menu
echo "DEFAULT    menu.c32
MENU TITLE Server PXE Boot Menu
TIMEOUT    100
" > /var/lib/tftpboot/pxelinux.cfg/default

# Add memtest86
wget https://www.memtest.org/download/5.31b/memtest86+-5.31b.bin.gz -O /tmp/mt86plus.gz
gzip -d /tmp/mt86plus
mkdir /var/lib/tftpboot/memtest
mv /tmp/mt86plus /var/lib/tftpboot/memtest/
echo "LABEL      memtest
MENU LABEL ^Memtest86+ v5.31b
KERNEL     memtest/mt86plus
" >> /var/lib/tftpboot/pxelinux.cfg/default

# Add DBAN Autonuke
wget https://downloads.sourceforge.net/project/dban/dban/dban-2.3.0/dban-2.3.0_i586.iso?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fdban%2Ffiles%2Fdban%2Fdban-2.3.0%2Fdban-2.3.0_i586.iso%2Fdownload&ts=1608933465 -O /tmp/dban.iso
mkdir /tmp/dban
mount -o loop /tmp/dban.iso /tmp/dban
mkdir -p /var/lib/tftpboot/dban
cp /tmp/dban/dban.bzi /var/lib/tftpboot/dban/
umount /tmp/dban
echo "LABEL      dban-autonuke
MENU LABEL ^DBAN Autonuke!
KERNEL     dban/dban.bzi
APPEND     nuke=\"dwipe --autonuke\" silent
" >> /var/lib/tftpboot/pxelinux.cfg/default

# Add DBAN
curl https://downloads.sourceforge.net/project/dban/dban/dban-2.3.0/dban-2.3.0_i586.iso?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fdban%2Ffiles%2Fdban%2Fdban-2.3.0%2Fdban-2.3.0_i586.iso%2Fdownload&ts=1608933465 -o /var/lib/tftpboot/dban/dban.iso --create-dirs
echo "LABEL      dban
MENU LABEL ^DBAN v2.3.0
LINUX      memdisk
INITRD     dban/dban.iso
APPEND     iso
" >> /var/lib/tftpboot/pxelinux.cfg/default

# Add Parted Magic
curl -u $USER:$PASSWORD -X GET https://cloud.computertruhe.de/remote.php/dav/files/$USER/Allgemein/Software/Datenvernichtung/pmagic_2020_10_12.iso -o /tmp/pmagic.iso
cd /tmp && mkdir pmagic
mount -o loop /tmp/pmagic.iso /tmp/pmagic
sh /tmp/pmagic/boot/pxelinux/pm2pxe.sh
cp -v pmagic/pmagic/{bzImage,initrd.img,fu.img,m.img} /var/lib/tftpboot/pmagic/
cp pm2exe/files.cgz /var/lib/tftpboot/pmagic/
umount /tmp/pmagic
echo "LABEL      pmagic
MENU LABEL ^Parted Magic 2020-10-12
LINUX      pmagic/bzImage
INITRD     pmagic/initrd.img,pmagic/fu.img,pmagic/m.img,pmagic/files.cgz
APPEND     edd=on vga=normal
" >> /var/lib/tftpboot/pxelinux.cfg/default

# Add MINT
curl http://ftp-stud.hs-esslingen.de/pub/Mirrors/linuxmint.com/stable/20/linuxmint-20-cinnamon-64bit.iso -o /tmp/mint.iso
mkdir /tmp/mint
mount -o loop /tmp/mint.iso /tmp/mint
cp -rT /tmp/mint /var/lib/tftpboot/mint 
umount /tmp/mint
echo "LABEL      mint
MENU LABEL ^Linux MINT 20 Cinnamon
KERNEL     mint/casper/vmlinuz
APPEND     boot=casper vga=normal ip=dhcp netboot=nfs nfsroot=$IP:/var/lib/tftpboot/mint initrd=mint/casper/initrd.lz splash --

LABEL      mint-oem
MENU LABEL ^Linux MINT 20 Cinnamon OEM
KERNEL     mint/casper/vmlinuz
APPEND     boot=casper vga=normal ip=dhcp netboot=nfs nfsroot=$IP:/var/lib/tftpboot/mint initrd=mint/casper/initrd.lz locale=de_DE keyboard-configuration/layoutcode=de console-setup/layoutcode=de only-ubiquity oem-config/enable=true quiet splash --
" >> /var/lib/tftpboot/pxelinux.cfg/default

# Restart service
chown -R nobody:nogroup /var/lib/tftpboot && chmod -R 755 /var/lib/tftpboot/ && systemctl restart dnsmasq